class Group < ApplicationRecord
    has_many :contacts
    belongs_to :user

    validates :name, presence: true
    # validates :name, uniqueness: true
    validates :name, uniqueness: { scope: [:name, :user_id] }
end
