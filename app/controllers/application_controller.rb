class ApplicationController < ActionController::Base
    include Pundit
    protect_from_forgery with: :exception
    before_action :configure_permitted_parameters, if: :devise_controller?
    rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized
    rescue_from ActiveRecord::RecordNotFound, with: :show_404
    helper_method :previous_query_string, :safe_paginate_params

    def after_sign_in_path_for(resource)
        stored_location_for(resource) || dashboard_path
    end

    def after_sign_up_path_for(resource)
        after_sign_in_path_for(resource)
    end

    def safe_paginate_params
        { contact: nil, new_group: nil, action: "index", id: nil, "X-Http-Accept": nil, "X-Requested-With": nil, "remotipart_submitted": nil}
    end

    def previous_query_string
        # params.permit(:page, :group_id, :term, :contact, :new_group)
        
        new_params = params.select {|key| ["page", "group_id"].include?(key) }
        new_params.permit!
        new_params.to_h
        
        # ActionController::Parameters.permit_all_parameters = true
        # params = ActionController::Parameters.new(new_params.to_h)
    end

    private

    def user_not_authorized
        flash[:danger] = "You are not authorized to perform this action."
        redirect_to action: :index
    end

    def show_404
        render template: "errors/404", status: 404
    end

    protected

    def configure_permitted_parameters
        devise_parameter_sanitizer.permit(:sign_up, keys: [:name])
        devise_parameter_sanitizer.permit(:account_update, keys: [:name])
    end
end
